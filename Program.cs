﻿using System;

namespace Uninherited
{
    public interface IUninherited<DataType>
    {

    }

    public static class UninheritedExtensions
    {
        public static void Do<DataType>( this DataType foo )
            where DataType : IUninherited<DataType>
        {
            Console.WriteLine( $"Hello, world! I'm a '{foo.GetType()}'." );
        }
    }

    public class B : IUninherited<B>
    {

    }

    public class A : B
    {

    }

    class Program
    {
        static void Main(string[] args)
        {
            B x = new B( );
            x.Do( );

            A y = new A( );
            // Nope!
            // y.Do( );
            (y as B).Do( );
        }
    }
}
